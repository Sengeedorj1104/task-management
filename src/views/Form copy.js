// import { render } from "node-sass";
// import React, { useState, useEffect } from 'react';
import React, { Component, useState } from "react";

import {
  FormGroup,
  Label,
  Input,
  FormText,
  Button,
  Card,
  CardBody
} from "reactstrap";
import axios from "axios";

export default function Forms() {


  this.state = {
    email: '',
    phone: '',
    address: ''

  }

  const [postData, setPostData] = useState({
    customerName: '', phone: '', email: '', address: '', status: 'status',
    price: '', type: ''
  });

  const handleChange = event => {
    console.log("change");
    setState({ name: event.target.value });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const user = postData

    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'JWT fefege...'
    }
    console.log(user);
    axios.post("http://localhost:8080/task", { user }, {
      headers: headers
    })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })

    fetch('http://localhost:8080/task', {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringfy(this.user)
    })
      .then(r => r.json()
      )

  };

  changeHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }


  submitHandler = e => {
    e.preventDefault()
    console.log(this.state)
    axios.post('http://localhost:8080/task', this.state)
      .then(reponse => {
        console.log(response)
      })
      .catch(error => {
        console.log(error)
      })
  }


  return (
    <Card className="content">
      <CardBody>
        <form onSubmit={this.submitHandler} >
          <FormGroup>
            <Label for="exampleEmail">Email address</Label>
            <Input
              type="email"
              name="email"
              id="exampleEmail"
              placeholder="Enter email"
              // value={user.name}
              // onChange={(e) => setPostData({ ...postData, email: e.target.value })}
              onChange={this.changeHandler}
            />
            <FormText color="muted">
              We'll never share your email with anyone else.
            </FormText>
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">Phone</Label>
            <Input
              type="phone"
              name="phone"
              // id="examplePassword"
              placeholder="phone"
              autoComplete="off"
              onChange={(e) => setPostData({ ...postData, phone: e.target.value })}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">customerName</Label>
            <Input
              type="customerName"
              name="customerName"
              // id="examplePassword"
              placeholder="customerName"
              autoComplete="off"
              onChange={(e) => setPostData({ ...postData, customerName: e.target.value })}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">status</Label>
            <Input
              type="status"
              name="status"
              // id="examplePassword"
              placeholder="status"
              autoComplete="off"
              onChange={(e) => setPostData({ ...postData, status: e.target.value })}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">address</Label>
            <Input
              type="address"
              name="address"
              // id="examplePassword"
              placeholder="address"
              autoComplete="off"
              onChange={(e) => setPostData({ ...postData, address: e.target.value })}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">price</Label>
            <Input
              type="price"
              name="price"
              // id="examplePassword"
              placeholder="price"
              autoComplete="off"
              onChange={(e) => setPostData({ ...postData, price: e.target.value })}
            />
          </FormGroup>
          <FormGroup check>
            <Label check>
              <Input type="checkbox" />{' '}
              Check me out
              <span className="form-check-sign">
                <span className="check"></span>
              </span>
            </Label>
          </FormGroup>
          <Button color="primary">
            Submit
          </Button>
        </form>
      </CardBody>
    </Card>
  );

};
