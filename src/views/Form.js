// import { render } from "node-sass";
// import React, { useState, useEffect } from 'react';
import React, { useState } from "react";

import {
  FormGroup,
  Label,
  Input,
  FormText,
  Button,
  Card,
  CardBody
} from "reactstrap";
import axios from "axios";


export default function Forms() {
  const [state, setState] = useState({
    customerName: 'customerName', phone: '99738872', email: 'sengee@gmail.com'
    , address: 'address', status: 'new', asignee: 'sengeedorj',
    type: 'survey', price: 5
  });

  const changeHandler = e => {
    console.log(state)
    setState({ ...state, [e.target.name]: e.target.value })
  }

  const cancelCourse = () => {
    document.getElementById("create-course-form").reset();
  }

  const submitHandler = e => {
    e.preventDefault()
    console.log(state)
    axios.post('http://localhost:8080/task', state)
      .then(res => { console.log(res) })
      .catch(error => {
        console.log(error)
      })

    cancelCourse();
  }


  return (
    <Card className="content">
      <CardBody>
        <form onSubmit={submitHandler} id="create-course-form">
          <FormGroup>
            <Label >Нэр</Label>
            <Input
              // type="customerName"
              name="customerName"
              // id="examplePassword"
              placeholder="customerName"
              autoComplete="off"
              onChange={changeHandler}
            />
          </FormGroup>
          <FormGroup>
            <Label >Майл хаяг</Label>
            <Input
              type="email"
              name="email"
              id="exampleEmail"
              placeholder="Enter email"
              // value={user.name}
              // onChange={(e) => setPostData({ ...postData, email: e.target.value })}
              onChange={changeHandler}
            />
            {/* <FormText color="muted">
              We'll never share your email with anyone else.
            </FormText> */}
            <FormGroup>
              <Label >Утас</Label>
              <Input
                type="phone"
                name="phone"
                // id="examplePassword"
                placeholder="phone"
                autoComplete="off"
                onChange={changeHandler}
              />
            </FormGroup>
            {/* <FormGroup>
              <Label >Томилсон</Label>
              <Input
                type="asignee"
                name="asignee"
                // id="examplePassword"
                placeholder="asignee"
                autoComplete="off"
                onChange={changeHandler}
              />
            </FormGroup> */}
            <FormGroup>
              <Label >Хаяг</Label>
              <Input
                type="address"
                name="address"
                // id="examplePassword"
                placeholder="address"
                autoComplete="off"
                onChange={changeHandler}
              />
            </FormGroup>
            <FormGroup>
              <Label >Үнэ</Label>
              <Input
                type="price"
                name="price"
                // id="examplePassword"
                placeholder="price"
                autoComplete="off"
                onChange={changeHandler}
              />
            </FormGroup>
          </FormGroup>
          <Button color="primary">
            Илгээх
          </Button>
        </form>
      </CardBody>
    </Card>
  );

};
