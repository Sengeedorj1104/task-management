import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
} from "reactstrap";

export default function Tables() {
  const [state, setState] = useState({
    data: [],
  });

  const [taskStatus, setTaskStatus] = useState('');


  useEffect(() => {
    axios.get("http://localhost:8080/task?asignee=sengeedorj")
      .then((result) => setState({ data: result.data }))
      .catch((err) => console.log(err.message));
  }, []);

  const changeHandler = e => {
    e.preventDefault();
    setTaskStatus(e.target.value);
    console.log(taskStatus)
  }

  const updateHandler = async (e) => {

    e.preventDefault();
    if (taskStatus !== '') {
      axios.post("http://localhost:8080/updatetask?tasknum=" + e.target.id + "&status=" + taskStatus)
        .then(res => { console.log(res) })
        .catch(error => {
          console.log(error)
        });

      setTaskStatus('');



      axios.get("http://localhost:8080/task?asignee=sengeedorj")
        .then((result) => setState({ data: result.data }))
        .catch((err) => console.log(err.message));

    }
  };



  return (
    <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <tr>
                <CardHeader>
                  <CardTitle tag="h4">Ажлын даалгаварууд</CardTitle>
                </CardHeader>
              </tr>
              <CardBody>
                <Table className="tablesorter" responsive>
                  <thead className="text-primary">
                    <tr>
                      <th>Нэр</th>
                      <th>Үнэ</th>
                      <th>Утас</th>
                      <th>Хаяг</th>
                      <th >Төлөв</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      state.data.map((el) => (

                        <tr>
                          <td>{el.customerName}</td>
                          <td>{el.price}</td>
                          <td>{el.phone}</td>
                          <td>{el.address}</td>
                          <td>
                            <select value={el.status} onChange={changeHandler}  >
                              <option value="new">new</option>
                              <option value="pending">pending</option>
                              <option value="working progress">working progress</option>
                              <option value="done">done</option>
                            </select>
                          </td>


                          <td><a href="#" onClick={updateHandler} id={el.id} >
                            Шинчлэх
                          </a></td>
                        </tr>
                      ))
                    }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );

}